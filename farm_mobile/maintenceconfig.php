<?php  defined('SYS_PATH') or die('No direct script access.');
return  array (
  'admin_udids' => array('2373ac559e324ac56d1e2649c9b23013',
  '8c340f2b43d44bfba8d92bf0a7b46517',
  'b093b4cbfdc8369be3261abfdb4cb1b3',
  '397dbe6e78d75cec59e25e33decc4d44'),
  'start_uid' => 1,
  'end_uid' => 11000000,
  'maintence_msg' => 
  array (
    'en_US' => 
    array (
		'title' => 'Dear user!',
		'msg' => 'The farm is in maintenance right now. Please take a break and it will be back very soon.  Have a good day',
    ),
    'en' => 
    array (
		'title' => 'Dear user!',
		'msg' => 'The farm is in maintenance right now. Please take a break and it will be back very soon.  Have a good day',
    ),
  	'fr' =>
  	array (
  		'title' => 'Ami Fermier!',
  		'msg' => 'La ferme est actuellement en maintenance. Merci de ta patience, nous serons de retour très bientôt. Passe une bonne journée!',
  		),
  		'de' =>
  		array (
  				'title' => 'Liebe Spieler!',
  				'msg' => 'Die Farm wird momentan verbessert. Bitte nimm dir eine kurze Pause, wir sind bald wieder zurück. Hab einen schönen Tag!',
  		),
  		'jp' =>
  		array (
  				'title' => 'こんにちは',
  				'msg' => 'ただいまサーバーメンテナンス中だわ。もうすぐゲームに入れるよ。ちょっと待ってね。',
  		),
  		'nl' =>
  		array (
  				'title' => 'Beste speler!',
  				'msg' => 'We zijn op dit moment je boerderij aan het onderhouden. Wacht heel even, het is zo weer beschikbaar. Een prettige dag verder!',
  				),
  		'pt' =>
  		array (
  				'title' => 'Querido Fazendeiro!',
  				'msg' => 'A Fazenda está em manutenção. Por favor, espere um pouquinho. Logo, logo estaremos de volta!',
  		),
  		'tr' =>
  		array (
  				'title' => 'Sevgili Oyuncu!',
  				'msg' => 'Şu anda sunucularımızda bir bakım yapılmakta. Lütfen biraz bekledikten sonra yeniden deneyiniz.İyi eğlenceler!',
  		),
  		'th' =>
  		array (
  				'title' => 'ถึงผู้เล่นที่รัก!',
  				'msg' => 'ฟาร์มของเรากำลังได้รับการซ่อมแซมแก้ไข โปรดรอหน่อย อีกเดี๋ยวจะกลับมาใหม่ ยังไงก็ขอให้มีวันที่สดใสนะจ๊ะ!',
  		),
  		'tw' =>
  		array (
  				'title' => '親愛的玩家',
  				'msg' => '伺服器睡着了ZZZzzzz... 我們的工程師正在叫醒他...',
  		),
  		'es' =>
  		array (
  				'title' => '¡Querido usuário!',
  				'msg' => 'La granja está bajo mantenimiento al momento. Por favor, espera un rato, estará de regreso muy pronto. ¡Que tengas un buen día!',
  		),
  		'pl' =>
  		array (
  				'title' => 'Drogi graczu!',
  				'msg' => 'Farma jest teraz w konserwacji. Zrób sobie krótką przerwę, wrócimy lada chwila! Miłego dnia!',
  		),
  		'it' =>
  		array (
  				'title' => 'Caro giocatore',
  				'msg' => 'La fattoria è momentaneamente in manutenzione. Ti preghiamo di attendere, tornerà molto presto! Buona giornata!',
  		),
  		'ae' =>
  		array (
  				'title' => 'اعزائى اللاعبين!',
  				'msg' => 'اللعبة الان تحت الصيانة ، منفضلك انتظر لبعض الوقت ، سوف نعود قريبا ، مع تمنياتنا لكم بالتوفيق !',
  		),
  		'ko' =>
  		array (
  				'title' => '사랑하는 친구들',
  				'msg' => '서버를 보수하고 있습니다. 잠시만 기다려 주십시오. 좋은 하루 보내세요!',
  		),
    'nb' => 
    array (
		'title' => 'Dear user!',
		'msg' => 'The farm is in maintenance right now. Please take a break and it will be back very soon.  Have a good day',
    ),
    /*1 => 
    array (
      'name' => 'Franch',
      'backend_lang' => 'fr',
      'front_lang' => 'fr',
    ),
    2 => 
    array (
      'name' => 'German',
      'backend_lang' => 'de',
      'front_lang' => 'de',
    ),
    3 => 
    array (
      'name' => 'Japanese',
      'backend_lang' => 'jp',
      'front_lang' => 'ja',
    ),
    4 => 
    array (
      'name' => 'Dutch',
      'backend_lang' => 'nl',
      'front_lang' => 'nl',
    ),
    5 => 
    array (
      'name' => 'Portuguese',
      'backend_lang' => 'pt',
      'front_lang' => 'pt',
    ),
    6 => 
    array (
      'name' => 'Turkish',
      'backend_lang' => 'tr',
      'front_lang' => 'tr',
    ),
    7 => 
    array (
      'name' => 'Thai',
      'backend_lang' => 'th',
      'front_lang' => 'th',
    ),
    8 => 
    array (
      'name' => 'Chinese Traditional',
      'backend_lang' => 'tw',
      'front_lang' => 'zh-Hant',
    ),
    9 => 
    array (
      'name' => 'Italian',
      'backend_lang' => 'it',
      'front_lang' => 'it',
    ),
    10 => 
    array (
      'name' => 'Polish',
      'backend_lang' => 'pl',
      'front_lang' => 'pl',
    ),*/
  ),
);
