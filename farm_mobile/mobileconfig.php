<?php  defined('SYS_PATH') or die('No direct script access.');
return  array (
		'max_neighbor_limit' => 350,
		'sell_unlock_level' => 8,     //卖东西用户解锁级别
		'pulling_back_days' => 1,     //3天不活跃的用户
		'max_pull_back_times' => 10,     //
		'enable_galastory'=>true,
		'platform_reward' =>
		array (
			'coins' => 10,
			'reward_points' => 3,
		      ),
		'neighbor_harvest'=>array(
			'max_falling_count' => 50,
			'init_percent' => 20,
			),
		'power_interval'=>600,
		'power_increment'=>1,
		'power_limit'=>30,
		'notification_interval' => 43200,
		'APN_time' => '10:00:00-22:00:00',
		'payment_callback_url' => 'http://payment-sandbox.socialgamenet.com/callback/appleiap/',
'payment_appid' => '51',
	'google_play_payment_callback_url' => 'http://payment-sandbox.socialgamenet.com/callback/googleplayiap/',
	'google_play_payment_appid' => '54',
	'save_data_interval' => 8,
	'app_version' => '1.1',
	'ios_app_version' => '1.3',
	'google_play_app_version' => '1.4',
	'show_add_neighbor_dock_count_limit' => 5,
	'resource_server' => 'http://d1u8w83vclyuyy.cloudfront.net',
	//'resource_server' => 'http://farm-mobile-asset.s3.amazonaws.com',
	'fb_app_id' => '240533579330356',
	'feed_link' => 'http://bit.ly/ffseaside',
	//'app_page_link' => 'http://testflightapp.com/dashboard/',
	'app_page_link' => 'https://itunes.apple.com/us/app/family-farm-seaside/id539920547?ls=1&mt=8',
	'ios_app_page_link' => 'https://itunes.apple.com/us/app/family-farm-seaside/id539920547?ls=1&mt=8',
	'app_review_link' => 'itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=539920547',
	'google_play_app_page_link' => 'https://itunes.apple.com/au/app/family-farm-seaside/id539920547?mt=8&uo=4',
	'google_play_app_review_link' => 'itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=539920547',
	'ad_api_url' => 'http://192.168.33.10/mobile_loader.php',
	'showMysticalGift' => false,
	'isShowRating' => true,//评分面板
	'close_opengraph' => 0,//
	'closed_opengraph_actions' => array('upgrade'),
	'special_characters' => array('\\',"'",'"','/',':','�','︻','╦̴','╦','═'),
	'discount_panel'=>array('first'=>array('is_shown_rcpopup'=>true,//控制打折活动面板是否自动弹出
				'is_shown_promotionicon'=>true),//控制打折活动的icon是否显示
			'normal'=>array('is_shown_rcpopup'=>true,//控制普通打折活动面板是否自动弹出
				'is_shown_promotionicon'=>true),//控制普通打折活动的icon是否显示
			),
	'log'=>array(
			"client_watch_level"=>'1',
			"client_watch_filter"=>'add_object,remove_object',
			"appname"=>"FFS",
			"als_server"=> "http://ec2-54-228-215-81.eu-west-1.compute.amazonaws.com/",
			"als_secret_key"=>"}+5eHr,[EHh{M1#O%_kC:3%oTXaAxJIs^VuS2{>H8.vkZgf%q3",
			"als_app_alias"=>"44EbFH",
			"als_default_log_category"=>"ffs_mobile",
			"als_crash_log_category"=>"ffs_mobile_crash",
			"als_log_level"=>"info"
		    ),
	'unavailable_languages'=>array(),
	'language_selects' => array (
			array (
				'name' => 'English',
				'backend_lang' => 'en_US',
				'front_lang' => 'en',
			      ),
			array (
				'name' => 'Franch',
				'backend_lang' => 'fr',
				'front_lang' => 'fr',
			      ),
			array (
				'name' => 'German',
				'backend_lang' => 'de',
				'front_lang' => 'de',
			      ),
			array (
				'name' => 'Japanese',
				'backend_lang' => 'jp',
				'front_lang' => 'ja',
			      ),
			array (
					'name' => 'Dutch',
					'backend_lang' => 'nl',
					'front_lang' => 'nl',
			      ),
			array (
					'name' => 'Portuguese',
					'backend_lang' => 'pt',
					'front_lang' => 'pt',
			      ),
			array (
					'name' => 'Turkish',
					'backend_lang' => 'tr',
					'front_lang' => 'tr',
			      ),
			array (
					'name' => 'Thai',
					'backend_lang' => 'th',
					'front_lang' => 'th',
			      ),
			array (
					'name' => 'Chinese Traditional',
					'backend_lang' => 'tw',
					'front_lang' => 'zh-Hant',
			      ),
			array (
					'name' => 'Italian',
					'backend_lang' => 'it',
					'front_lang' => 'it',
			      ),
			array (
					'name' => 'Polish',
					'backend_lang' => 'pl',
					'front_lang' => 'pl',
			      ),
			array (
					'name' => 'Spanish',
					'backend_lang' => 'es',
					'front_lang' => 'es',
			      ),
			array (
					'name' => 'Norwegian',
					'backend_lang' => 'nb',
					'front_lang' => 'nb',
			      ),
			array (
					'name' => 'Arabic',
					'backend_lang' => 'ae',
					'front_lang' => 'ar-AE',
			      ),
			),
			'daily_gifts_rules' =>
			array (
					0 =>
					array (
						1 =>
						array (
							0 =>
							array (
								'type' => 'fertilizer',
								'amount' => 10,
								'id' => 10000,
								'min' => 1,
								'max' => 30,
							      ),
							1 =>
							array (
								'type' => 'gas',
								'amount' => 2,
								'min' => 31,
								'max' => 60,
							      ),
							2 =>
							array (
								'type' => 'op',
								'amount' => 30,
								'min' => 61,
								'max' => 90,
							      ),
							3 =>
							array (
									'type' => 'reward_points',
									'amount' => 1,
									'min' => 91,
									'max' => 100,
							      ),
							),
							2 =>
							array (
									0 =>
									array (
										'type' => 'fertilizer',
										'amount' => 25,
										'id' => 10000,
										'min' => 1,
										'max' => 30,
									      ),
									1 =>
									array (
										'type' => 'gas',
										'amount' => 5,
										'min' => 31,
										'max' => 60,
									      ),
									2 =>
									array (
										'type' => 'op',
										'amount' => 50,
										'min' => 61,
										'max' => 90,
									      ),
									3 =>
									array (
											'type' => 'reward_points',
											'amount' => 2,
											'min' => 91,
											'max' => 100,
									      ),
									),
									3 =>
									array (
											0 =>
											array (
												'type' => 'fertilizer',
												'amount' => 40,
												'id' => 10000,
												'min' => 1,
												'max' => 30,
											      ),
											1 =>
											array (
												'type' => 'gas',
												'amount' => 8,
												'min' => 31,
												'max' => 60,
											      ),
											2 =>
											array (
												'type' => 'op',
												'amount' => 80,
												'min' => 61,
												'max' => 90,
											      ),
											3 =>
											array (
													'type' => 'reward_points',
													'amount' => 2,
													'min' => 91,
													'max' => 100,
											      ),
											),
											4 =>
											array (
													0 =>
													array (
														'type' => 'fertilizer',
														'amount' => 60,
														'id' => 10000,
														'min' => 1,
														'max' => 30,
													      ),
													1 =>
													array (
														'type' => 'gas',
														'amount' => 10,
														'min' => 31,
														'max' => 60,
													      ),
													2 =>
													array (
														'type' => 'op',
														'amount' => 100,
														'min' => 61,
														'max' => 90,
													      ),
													3 =>
													array (
															'type' => 'reward_points',
															'amount' => 3,
															'min' => 91,
															'max' => 100,
													      ),
													),
													),
													1 =>
													array (
															'type' => 'coins',
															'amount' => 100,
													      ),
													2 =>
													array (
															'type' => 'coins',
															'amount' => 150,
													      ),
													3 =>
													array (
															'type' => 'coins',
															'amount' => 200,
													      ),
													4 =>
													array (
															'type' => 'coins',
															'amount' => 250,
													      ),
													),
													'ad' => array(
															'config'=>array(
																'key' => 'f3#kjdijfdk_kdjfkie$%)(abckFIQ23',
																'type'  => 1,   //1:video   2:activity
																'zone_id' => 'vz914a644e94f54be79004ab',
																'zone_no' => 2,
																'level'=>10,
																'rc'	=> 20,
																'reward_rc' => 3,
																'start_time'	=>'1371087294',
																'end_time'		=>'1371177294',
																'daily_display_times'	=>5,
																'language'		=>'en',
																'max_get_times'	=>3,
																),
															'userdata' => array(
																'got_times' => 0,
																)

														     )
													)
													?>
