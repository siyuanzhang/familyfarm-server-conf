<?php
defined('SYS_PATH') OR die('No direct access allowed.');
return array (
	'default' => array (),
	'db0'	  => array(
		'host'		 => '10.244.151.247',
		'port'		 => '27017',
		'replicaSet' => 'sgn1',
		'database'	 => 'farm_0',
	),
	'db1'	  => array(
		'host'		 => '10.244.151.247',
		'port'		 => '27017',
		'replicaSet' => 'sgn1',
		'database'	 => 'farm_1',
	),
);

