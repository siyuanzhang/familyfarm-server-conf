<?php
return array (
		'sgnTypes' => array (
				'appleiap' => array (
						'com.funplus.familyfarm.coins1600' => array (
								'type' => 'currency',
								'kind' => 'coins',
								'quantity' => 1600 
						),
						'com.funplus.familyfarm.rc12' => array (
								'type' => 'currency',
								'kind' => 'rc',
								'quantity' => 12
						),
						'com.funplus.familyfarm.coins4500' => array (
								'type' => 'currency',
								'kind' => 'coins',
								'quantity' => 4500
						),
						'com.funplus.familyfarm.coins10000' => array (
								'type' => 'currency',
								'kind' => 'coins',
								'quantity' => 10000
						),
						'com.funplus.familyfarm.coins22000' => array (
								'type' => 'currency',
								'kind' => 'coins',
								'quantity' => 22000
						),
						'com.funplus.familyfarm.rc35' => array (
								'type' => 'currency',
								'kind' => 'rc',
								'quantity' => 35
						),
						'com.funplus.familyfarm.rc75' => array (
								'type' => 'currency',
								'kind' => 'rc',
								'quantity' => 75
						),
						'com.funplus.familyfarm.rc160' => array (
								'type' => 'currency',
								'kind' => 'rc',
								'quantity' => 160
						),
						'com.funplus.familyfarm.rc500' => array (
								'type' => 'currency',
								'kind' => 'rc',
								'quantity' => 500
						),
						'com.funplus.familyfarm.rc750' => array (
								'type' => 'currency',
								'kind' => 'rc',
								'quantity' => 750
						),
						'com.funplus.familyfarm.newpayer.first' => array (
								'type' => 'first_few_time_pay',
								'kind' => 'rc',
								'quantity' => 10
						),
						'com.funplus.familyfarm.newpayer.second' => array (
								'type' => 'first_few_time_pay',
								'kind' => 'rc',
								'quantity' => 30
						),
						'com.funplus.familyfarm.newpayer.third' => array (
								'type' => 'first_few_time_pay',
								'kind' => 'rc',
								'quantity' => 45
						),
						'com.funplus.familyfarm.newpayer.fourth' => array (
								'type' => 'first_few_time_pay',
								'kind' => 'rc',
								'quantity' => 110
						),
						'com.funplus.familyfarm.starterpack' => array (
								'type' => 'gift_package',
								'store_id' => 400014
						),
				),
				'googleplayiap' => array (
						'com.funplus.familyfarm.coins.astack' => array (
								'type' => 'currency',
								'kind' => 'coins',
								'quantity' => 1600 
						),
						'com.funplus.familyfarm.rc.abunch' => array (
								'type' => 'currency',
								'kind' => 'rc',
								'quantity' => 12
						),
						'com.funplus.familyfarm.coins.apouch' => array (
								'type' => 'currency',
								'kind' => 'coins',
								'quantity' => 4500
						),
						'com.funplus.familyfarm.coins.asack' => array (
								'type' => 'currency',
								'kind' => 'coins',
								'quantity' => 10000
						),
						'com.funplus.familyfarm.coins.achest' => array (
								'type' => 'currency',
								'kind' => 'coins',
								'quantity' => 22000
						),
						'com.funplus.familyfarm.rc.astack' => array (
								'type' => 'currency',
								'kind' => 'rc',
								'quantity' => 35
						),
						'com.funplus.familyfarm.rc.apouch' => array (
								'type' => 'currency',
								'kind' => 'rc',
								'quantity' => 75
						),
						'com.funplus.familyfarm.rc.asack' => array (
								'type' => 'currency',
								'kind' => 'rc',
								'quantity' => 160
						),
						'com.funplus.familyfarm.rc.abox' => array (
								'type' => 'currency',
								'kind' => 'rc',
								'quantity' => 500
						),
						'com.funplus.familyfarm.rc.achest' => array (
								'type' => 'currency',
								'kind' => 'rc',
								'quantity' => 750
						),
						'com.funplus.familyfarm.newpayer.first' => array (
								'type' => 'first_few_time_pay',
								'kind' => 'rc',
								'quantity' => 10
						),
						'com.funplus.familyfarm.newpayer.second' => array (
								'type' => 'first_few_time_pay',
								'kind' => 'rc',
								'quantity' => 30
						),
						'com.funplus.familyfarm.newpayer.third' => array (
								'type' => 'first_few_time_pay',
								'kind' => 'rc',
								'quantity' => 45
						),
						'com.funplus.familyfarm.newpayer.fourth' => array (
								'type' => 'first_few_time_pay',
								'kind' => 'rc',
								'quantity' => 110
						),
						'com.funplus.familyfarm.starterpack' => array (
								'type' => 'gift_package',
								'store_id' => 400014
						),
				),
				'amazoniap' => array (
						'com.funplus.familyfarm.coins.astack' => array (
								'type' => 'currency',
								'kind' => 'coins',
								'quantity' => 1600 
						),
						'com.funplus.familyfarm.rc.abunch' => array (
								'type' => 'currency',
								'kind' => 'rc',
								'quantity' => 12
						),
						'com.funplus.familyfarm.coins.apouch' => array (
								'type' => 'currency',
								'kind' => 'coins',
								'quantity' => 4500
						),
						'com.funplus.familyfarm.coins.asack' => array (
								'type' => 'currency',
								'kind' => 'coins',
								'quantity' => 10000
						),
						'com.funplus.familyfarm.coins.achest' => array (
								'type' => 'currency',
								'kind' => 'coins',
								'quantity' => 22000
						),
						'com.funplus.familyfarm.rc.astack' => array (
								'type' => 'currency',
								'kind' => 'rc',
								'quantity' => 35
						),
						'com.funplus.familyfarm.rc.apouch' => array (
								'type' => 'currency',
								'kind' => 'rc',
								'quantity' => 75
						),
						'com.funplus.familyfarm.rc.asack' => array (
								'type' => 'currency',
								'kind' => 'rc',
								'quantity' => 160
						),
						'com.funplus.familyfarm.rc.abox' => array (
								'type' => 'currency',
								'kind' => 'rc',
								'quantity' => 500
						),
						'com.funplus.familyfarm.rc.achest' => array (
								'type' => 'currency',
								'kind' => 'rc',
								'quantity' => 750
						),
						'com.funplus.familyfarm.newpayer.first' => array (
								'type' => 'first_few_time_pay',
								'kind' => 'rc',
								'quantity' => 10
						),
						'com.funplus.familyfarm.newpayer.second' => array (
								'type' => 'first_few_time_pay',
								'kind' => 'rc',
								'quantity' => 30
						),
						'com.funplus.familyfarm.newpayer.third' => array (
								'type' => 'first_few_time_pay',
								'kind' => 'rc',
								'quantity' => 45
						),
						'com.funplus.familyfarm.newpayer.fourth' => array (
								'type' => 'first_few_time_pay',
								'kind' => 'rc',
								'quantity' => 110
						),
						'com.funplus.familyfarm.starterpack' => array (
								'type' => 'gift_package',
								'store_id' => 400014
						),
				),
				'first_few_time_pay' => array (
						array (
								'store_id' => 400010,
								'rate' => 60 
						),
						array (
								'store_id' => 400011,
								'rate' => 45 
						),
						array (
								'store_id' => 400012,
								'rate' => 25 
						),
						array (
								'store_id' => 400013,
								'rate' => 15 
						) 
				)),
				// 'facebook_credits' => array('28'=>'18', '75'=>'53',
				// '150'=>'113',
				// '290'=>'240', '720'=>'750', '1400'=>'1650')
				
				'sgnSecret' => 'sf89&#$*(HFID8*&*JDD798edhfsd7s^*7DKLSJF*#DF*(i&3j',
				'sgnAppid' => 42,
				'payType' => 'sgn',
				'showIndexPaymentPage' => 'yes' 
		 
);

?>
