<?php defined('SYS_PATH') or die('No direct script access.');

return array (
    'mc_farm' => array(
        'driver'   => 'memcache',
        'servers'   => array (
            array (
                'host'        => '127.0.0.1',
                'port'        =>  11211,
                'persistent'  =>  false,
            ),
       ),
        'compression' => false,
        'lifetime'    => 3600,     // Default 1 hour
    ),
   'user' => array(
        'driver'   => 'memcache',
        'servers'   => array (
            array (
                'host'        => '127.0.0.1',
                'port'        =>  11211,
                'persistent'  =>  false,
            ),
       ),
        'compression' => false,
        'lifetime'    => 3600,     // Default 1 hour
    ),
   'neighbor' => array(
        'driver'   => 'memcache',
        'servers'   => array (
            array (
                'host'        => '127.0.0.1',
                'port'        =>  11211,
                'persistent'  =>  false,
            ),
       ),
        'compression' => false,
        'lifetime'    => 3600,     // Default 1 hour
    ),
);
