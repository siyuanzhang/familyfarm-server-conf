<?php
defined('SYS_PATH') OR die('No direct access allowed.');
return array (
	'default' => array (),
	'displayid1'     => array(
                'host'           => '127.0.0.1',
                'port'           => '6379',
                'replicaSet' => 'slave1',
                'timeout'       => 3
        ),
	'displayid2'     => array(
                'host'           => '127.0.0.1',
                'port'           => '6379',
                'replicaSet' => 'slave1',
                'timeout'       => 3
        ),
	'db1'     => array(
                'host'           => '127.0.0.1',
                'port'           => '6379',
                'replicaSet' => 'slave1',
                'timeout'       => 3
        ),
	'db2'	  => array(
		'host'		 => '127.0.0.1',
		'port'		 => '6379',
		'replicaSet' => 'slave1',
		'timeout'	=> 3
	),
);
