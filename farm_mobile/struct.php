<?php
return  array
(
	 'GameVersion'               => 'farm-mobile',
	 'game_no'                   => 5,
	 'version'                   => 'ff-ios',
	 'version_android'                   => 'ff-androi',
	 'trackUrl'                              => "http://bigdata.socialgamenet.com/44EbFH/",
	 'trackKey'                          => 'VRuhHY?-[i"&Gb$*EO<UKc=,4JCEjZ3q>?:?OZPgJaq(7WqDS.]FjN"3]5@T&$TP',
	'useNewStruct' => 'yes', //使用新的数据库结构
	'plingaValidate' => 'no', //是否使用plinga的验证
	'app_als'=>'44EbFH',
	'als_secret_key'=>'}+5eHr,[EHh{M1#O%_kC:3%oTXaAxJIs^VuS2{>H8.vkZgf%q3',
	 'als_url'    => "http://ec2-54-228-215-81.eu-west-1.compute.amazonaws.com/",
	'partionDb' => 'yes', //是否分库
	'partitionDbBase' => '200000', //分库的基数

	'partitionTableMap' => 'yes', //是否对MAP分表
	'partitionTableMapBase' => '20000', //MAP分表的基数

	'partitionSceneDb' => 'yes', //对scene分库
	'useIdMap' => 'yes',
	'idmapDbItem' => 'db0', //idmap 使用的DB

	'useInvite' => 'yes',
	'useNeighborDb' => 'yes', //neighbor 是否存储数据库
	'getNeighborFromDb' => 'yes', //获取neighbor是否查找用户表,否则只查找memcache
	'neighborMemcacheUpdate' => 0, //neighbor memcache 更新时间间隔

	'buyLogs' => 'yes', //记录FG使用日志
    'buyLogTypes' => array('remove_object', 'add_object'),
    'errorLogs' => 'yes',
    
	'notShowReceiveGift' => 'yes', //不需要在flash中显示接收礼物

	'open_automation' => 'yes',	//是否打开离线自动化
	
//	'showAd' => 'yes', //显示广告
//	'showFirstPayAd' => 'yes', //是否显示第一次支付的广告

	'giftsprejoinDbItem' => 'db1',                  //还未加入游戏的好友接收到的礼物存储在哪里
	'wishesprejoinDbItem' => 'db1',
    'datacountDbItem' => 'db1',

    'useTrack' => 'yes',
    'adtracks' => 'yes',
//    'debugLocate' => 'facebook_am', //debug目录(目前fr,th,am,nl版本开启debuglog)
    
//	'ilikeActivity'=>'2012-03-02',
//    'gaid' => 'UA-29447837-1', //google-analytics account
    
//    'login_log_snsid' => array('1742063410','664640037','100001848999908','100000352572078','714062512','1307413178','100000761394667','100002546311904','100001717752389','1408254524','597517351','1184354757'),
	
	//camera的配置
//	'amazon_s3_key'=>'AKIAIR5Z4MO6NPP4CNEQ',
//	'amazon_s3_secret'=>'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',

//    'cameraUseAmazonS3' => 'yes',//camera是否使用s3来存储
//	'cameraBucketName' =>'farm-am-fb-camera',//这个版本对应的存放camera图片的bucket的name，需要在使用之前进行定义
//	'localhostCameraDir' =>'/mnt/htdocs/camera/',//如果不使用s3,则需要将文件存储在lb服务器，在这里指明文件的存储目录

	'giftsStoreDbType'		   => 'mongo',	//礼物数据库类型
	'partionMongoDB'           => 'yes',
	'deviceIdDbItem'  => 'db0',
	'platformIdDbItem'  => 'db0',
	'displayIdDbItem'  => 'db0',

         'PCFarmPromotion' => 'yes',

	'mongoGiftsprejoinDbItem'  => 'db1',
	'mongoWishesprejoinDbItem' => 'db1',
	'splitMongoBase'           => 200000,
	'partionRedisDB'	=> 'yes',
	'app_ad'=>'app_ad',
	'useNewGifts'=>'yes',
	'splitRedisBase'	=>1200000,
	'callIdCheck'		=>'yes',
	'record_types'=>array('reward_points'=>0,'op'=>0,'coins'=>1000),
);
