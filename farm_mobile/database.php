<?php
defined('SYS_PATH') OR die('No direct access allowed.');

return array (
	'default' => array (),
	'db0' => array(
		'type'         => 'mysql',
		'host'         => '127.0.0.1',
		'port'         => '3306',
		'username'     => 'root',
		'password'     => '',
		'persistent'   => FALSE,
		'database'     => 'farm_0',
		'table_prefix' => 'tbl_',
		'charset'      => 'utf8',
	),
	'db1' => array(
		'type'         => 'mysql',
		'host'         => '127.0.0.1',
		'port'         => '3306',
		'username'     => 'root',
		'password'     => '',
		'persistent'   => FALSE,
		'database'     => 'farm_1',
		'table_prefix' => 'tbl_',
		'charset'      => 'utf8',
	),
	'app_ad' => array(
		'type'         => 'mysql',
		'host'         => '127.0.0.1',
		'port'         => '3306',
		'username'     => 'root',
		'password'     => '',
		'persistent'   => FALSE,
		'database'     => 'app_ad',
		'table_prefix' => 'tbl_',
		'charset'      => 'utf8',
	)

);
